class Helpers {
  constructor() {
    this.cookieCloser = "#onetrust-close-btn-container > button";
    this.acceptButton = ".privacy-notification__footer > .btns-one-small";
  }

  /**
   * Returns a random number from the given range.
   * @param min Minimum range value. Included.
   * @param max Maximum range value. Included.
   * @returns Random value from the given range.
   */
  randomizer(min, max) {
    this.result = Math.floor(Math.random() * (max - min + 1)) + min;
    return this.result;
  }
}

export default new Helpers();
