class SearchPage {
  constructor() {
    this.searchResultsText = ".search-results__text";
    this.searchResultElement = "#fixedGridSection > article";
  }
}

export default new SearchPage();
