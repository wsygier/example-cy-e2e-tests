class CategoryPage {
  constructor() {
    this.categoryName = "h1 > .title-section__text";
    this.categoryElements = "#fixedGridSection";
  }
}

export default new CategoryPage();
