class MainPage {
  constructor() {
    /* selectors */
    this.searchField = ".nav-search > #searchText";
    this.searchButton = ".nav-search > .btn-basic--small";
    this.recipesCarousel = "#insideScroll > ul";
    this.categoryFromCarousel = `${this.recipesCarousel} > li`;

    /* variables */
    this.searchQueries = ["Hamburger", "Hot Dog", "Pizza"];
  }
}

export default new MainPage();
