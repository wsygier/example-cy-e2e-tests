import Helpers from "../commons/Helpers";
import MainPage from "../pages/MainPage";
import SearchPage from "../pages/SearchPage";
import CategoryPage from "../pages/CategoryPage";

describe("Example Cypress end-to-end tests", () => {
  beforeEach(() => {
    cy.visit("/")
      .get(Helpers.cookieCloser)
      .click()
      .get(Helpers.acceptButton)
      .click();
  });

  it("should search for some product and match the search query", () => {
    const searchQuery =
      MainPage.searchQueries[
        Helpers.randomizer(0, MainPage.searchQueries.length - 1)
      ];

    cy.get(MainPage.searchField)
      .type(searchQuery, {
        delay: 100,
      })
      .get(MainPage.searchButton)
      .click()
      .get(SearchPage.searchResultsText)
      .should("contain.text", searchQuery);
  });

  it("should open random element from the carousel and match its name", () => {
    cy.get(MainPage.categoryFromCarousel)
      .eq(Helpers.randomizer(1, 5))
      .then(($category) => {
        const category = $category.get(0).innerText;

        cy.wrap($category)
          .click()
          .get(CategoryPage.categoryName)
          .should("have.text", `${category} Recipes`);
      });
  });

  it("should fail", () => {
    cy.get(MainPage.categoryFromCarousel)
      .eq(1)
      .click()
      .get(CategoryPage.categoryElements)
      .children()
      .should("have.length", "0");
  });
});
