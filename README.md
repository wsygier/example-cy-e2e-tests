# Cypress Example End-to-End tests by Apphawks

## Introduction
Hello! This repository contains Apphawks' example end-to-end tests written in JavaScript using Cypress as test framework. Here you can see how we write our end-to-end tests using this tool.

## Package includes:
* Cypress v. 4.2.0 (runner)
* ESLint with plugins (code linting)
* Prettier (code formatting)

## Requirements:
* Node (min. v. 12.8.1),
* npm (version shipped with Node will be good).

## Installation
(I hope you're familiar with terminal, as you're gonna need to work in it).

1. Install Node and npm (npm comes bundled with Node).
2. Create a folder where you want to write your tests.
3. Open your favorite terminal.
4. Navigate to that folder.
5. Run `npm install` to download all the required modules.

## Running Cypress
1. As soon as you're done downloading, run `npm run cypress:open` in the folder you installed Cypress to open runner.
2. From the list select either the spec file (named `ExampleTests.js`) or click `Run all specs` button.
3. Wait for Cypress to do its job.

**DISCLAIMER**
For the sake of this presentation - one test is set to fail, so don't be scared if it does.

## Project layout
Project basically consists of two main parts:
* `commons` - doesn't come with Cypress, this is a folder where we store shared data (in our case all of it is in the `Helpers.js` file)
* `integration` - comes with Cypress, this is the folder with all the tests. By default Cypress looks for them there.
* `pages` folder - doesn't come with Cypress, but since we follow Page Object Pattern (POP) we added it here. This folder holds all the selectors, variables (if necessary) and methods used throughout the tests associated with the certain page.

Unused, but left for layout's sake:
* `fixtures` - not used here, but its purpose is to store test data (e.g. logins, emails, passwords etc.).
* `support` - more specifically, the `commands.js` file, which you can use to create custom cypress methods.

## Troubleshooting
**Q:** I can't run npm install due to permission problems.
**A:** Adding `sudo` before `npm install` should help. Alternatively, consider installing Node and npm via your system's package manager (e.g. `brew` for macOS) to avoid it.

**Q:** I can't see any tests, where are they?
**A:** All the tests should be in the `integration` folder, mak sure it's not empty.

**Q:** What browsers can I use?
**A:** Google Chrome, Electron (both shipped with Cypress), Firefox (only beta support) and Microsoft Edge (Chromium version). You can select the browser near the "Run all specs" button before you select a spec file.

**Q:** I can't go back to spec list.
**A:** `Tests` button is broken, it has been reported to the Cypress team already. Just close te runner window or Alt-Tab to it.


© Copyright Apphawks 2020. All rights reserved.